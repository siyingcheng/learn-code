# 斐波那契数

## 定义

$$
f_n \begin{cases} 
f_0 &= &0 \\
f_1 &= &1 \\
&\vdots \\
f_n &= &f_{n - 1} + f_{n - 2}
\end{cases}
$$



## 递归实现

```java
public static long fibByRecursion(int n) {
    if (n < 0) {
        throw new InvalidParameterException("The parameter 'n' can not less than 0.");
    }
    if (n <= 1) {
        return n;
    }
    return fibByRecursion(n - 1) + fibByRecursion(n - 2);
}
```



## 循环实现

```java
public static long fibByLoop(int n) {
    if (n < 0) {
        throw new InvalidParameterException("The parameter 'n' can not less than 0.");
    }
    if (n <= 1) {
        return n;
    }
    long a = 0L;
    long b = 1L;
    for (int i = 2; i <= n; i++) {
        b += a;
        a = b - a;
    }
    return b;
}
```



## 时间复杂度对比

通过大O表示法：`fibByRecursion`为 O(N)， 而`fibByRecursion` 为 O( $N^2$ )，所以从时间复杂度考虑递归效率较差，当n = 45 时：

```shell
01:20:14.134 [main] [INFO ] TimeUtils:40 --- 【fibByRecursion】
01:20:14.137 [main] [INFO ] TimeUtils:41 --- 开始：01:20:14.136
01:20:19.905 [main] [INFO ] TimeUtils:45 --- 结束：01:20:19.905
01:20:19.906 [main] [INFO ] TimeUtils:46 --- 耗时：5.768 秒
01:20:19.906 [main] [INFO ] TimeUtils:47 --- ---------------------------------------
01:20:19.907 [main] [INFO ] TimeUtils:40 --- 【fibByLoop】
01:20:19.907 [main] [INFO ] TimeUtils:41 --- 开始：01:20:19.907
01:20:19.908 [main] [INFO ] TimeUtils:45 --- 结束：01:20:19.908
01:20:19.909 [main] [INFO ] TimeUtils:46 --- 耗时：0.0 秒
01:20:19.909 [main] [INFO ] TimeUtils:47 --- ---------------------------------------
fibByRecursion f(45) = 1134903170
fibByLoop  f(45) = 1134903170
```



