/* Copyright 2021 - 2021 siyingcheng@126.com All rights reserved. */

package org.robot.fibonacci;

import org.robot.utils.TimeUtils;

import java.security.InvalidParameterException;
import java.util.Optional;

/**
 * @author : Si YingCheng
 * @version : v0.0.1
 * @since : 2021/4/20 01:07
 **/
public class Fibonacci {
    public static void main(String[] args) {
        Optional<?> fibByRecursion = TimeUtils.run("fibByRecursion", () -> Optional.of(fibByRecursion(45)));
        Optional<?> fibByLoop = TimeUtils.run("fibByLoop", () -> Optional.of(fibByLoop(45)));
        fibByRecursion.ifPresent(o -> System.out.println("fibByRecursion f(45) = " + o));
        fibByLoop.ifPresent(o -> System.out.println("fibByLoop  f(45) = " + o));
    }

    /**
     * 通过循环求fibonacci第n项
     *
     * @param n fibonacci第n项
     * @return long: fibonacci第n项值
     */
    public static long fibByLoop(int n) {
        if (n < 0) {
            throw new InvalidParameterException("The parameter 'n' can not less than 0.");
        }
        if (n <= 1) {
            return n;
        }
        long a = 0L;
        long b = 1L;
        for (int i = 2; i <= n; i++) {
            b += a;
            a = b - a;
        }
        return b;
    }

    /**
     * 递归方法求fibonacci第n项
     *
     * @param n fibonacci第n项
     * @return long: fibonacci第n项值
     */
    public static long fibByRecursion(int n) {
        if (n < 0) {
            throw new InvalidParameterException("The parameter 'n' can not less than 0.");
        }
        if (n <= 1) {
            return n;
        }
        return fibByRecursion(n - 1) + fibByRecursion(n - 2);
    }

    /**
     * 斐波那契的线性代数解法 - 特征方程
     *
     * @param n: 第n项
     * @return {@link long}
     * @date 2021/7/20 0:40
     **/
    public static long fibByEquation(int n) {
        double factor = Math.sqrt(5);
        return (long) ((Math.pow((1 + factor) / 2, n) - Math.pow((1 - factor) / 2, n)) / factor);
    }
}
