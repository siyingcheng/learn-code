/* Copyright 2021 - 2021 siyingcheng@126.com All rights reserved. */

package org.robot.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author : Si YingCheng
 * @version : v0.0.1
 * @since : 2021/4/20 01:52
 **/
public class TimeUtils {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
    private static final Logger LOGGER = LogManager.getLogger(TimeUtils.class);

    public interface Task {
        Optional<?> execute();
    }

    /**
     * 检查任务运行时间
     *
     * @param title: 方法名称
     * @param task: 任务
     * @date 2021/7/20 0:33
     * @return {@link Optional<?>}
     **/
    public static Optional<?> run(String title, Task task) {
        if (task == null) {
            return Optional.empty();
        }
        if (title == null) {
            title = "";
        } else {
            title = "--- 【" + title + "】";
        }
        System.out.println(title);
        System.out.println("--- 开始：" + sdf.format(new Date()));
        long begin = System.currentTimeMillis();
        Optional<?> result = task.execute();
        long end = System.currentTimeMillis();
        System.out.println("--- 结束：" + sdf.format(new Date()));
        System.out.println("--- 耗时：" + (end - begin) / 1000.0 + " 秒");
        System.out.println("--- ---------------------------------------");
        return result;
    }
}
