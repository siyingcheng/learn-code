package org.robot;

import org.robot.linearlist.RobotArrayList;

public class Main {
    public static void main(String[] args) {
        RobotArrayList<Integer> list = new RobotArrayList<>();
        list.add(11);
        list.add(22);
        list.add(33);
        list.add(44);
        list.add(55);
        list.add(44);
        list.add(55);
        System.out.println(list.indexOf(44));
        System.out.println(list.lastIndexOf(44));
    }
}
