package org.robot.linearlist;

public class RobotLinkedList<E> implements IDynamicLinearTable<E> {
    private static final int ELEMENT_NOT_FOUND = -1;
    private int size;
    private Node<E> first;

    @Override
    public int size() {
        return size;
    }

    /**
     * 获取指定元素
     *
     * @param index: 索引
     * @date 2021/7/23 1:49
     * @return {@link E}
     **/
    @Override
    public E get(int index) {
        return node(index).element;
    }

    /**
     * 添加一个元素
     *
     * @param element: 要添加的元素
     * @date 2021/7/23 1:48
     **/
    @Override
    public void add(E element) {
        insert(size, element);
    }

    /**
     * 更新指定位置的元素
     *
     * @param index: 要更新的索引
     * @param element: 新的元素
     * @return {@link E} 旧的元素
     * @date 2021/7/23 1:45
     **/
    @Override
    public E set(int index, E element) {
        Node<E> node = node(index);
        E old = node.element;
        node.element = element;
        return old;
    }

    /**
     * 在指定索引位置插入元素
     *
     * @param index:   插入元素的索引
     * @param element: 插入的元素
     * @date 2021/7/23 1:31
     **/
    @Override
    public void insert(int index, E element) {
        rangeCheckForInsert(index);
        Node<E> node = new Node<E>(element, null);
        if (index == 0) {
            node.next = first;
            first = node;
        } else {
            Node<E> prev = node(index - 1);
            node.next = prev.next;
            prev.next = node;
        }
    }

    /**
     * 移出索引位置的元素
     *
     * @param index: 索引
     * @return {@link E}
     * @date 2021/7/23 1:20
     **/
    @Override
    public E remove(int index) {
        rangeCheck(index);
        E old;
        if (index == 0) {
            old = first.element;
            first = first.next;
        } else {
            Node<E> prev = node(index - 1);
            old = prev.next.element;
            prev.next = prev.next.next;
        }
        size--;
        return old;
    }

    /**
     * 获取索引位置的元素
     *
     * @param index: 索引
     * @return {@link E}
     * @date 2021/7/23 1:08
     **/
    private Node<E> node(int index) {
        rangeCheck(index);
        Node<E> node = first;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node;
    }

    /**
     * 删除元素
     *
     * @param element: 要删除的元素
     * @date 2021/7/23 1:25
     **/
    @Override
    public void remove(E element) {
        int index = indexOf(element);
        if (index == ELEMENT_NOT_FOUND) {
            return;
        }
        remove(index);
    }

    /**
     * 删除所有指定元素
     *
     * @param element: 要删除的元素
     * @date 2021/7/23 1:30
     **/
    @Override
    public void removeAll(E element) {
        int index = indexOf(element);
        while (index != ELEMENT_NOT_FOUND) {
            remove(index);
            index = indexOf(element);
        }
    }

    /**
     * 链表是否为空？
     *
     * @return {@link boolean}
     * @date 2021/7/23 1:15
     **/
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * 链表中是否包含指定元素
     *
     * @param element: 元素
     * @return {@link boolean}
     * @date 2021/7/23 1:25
     **/
    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    /**
     * 元素的索引
     *
     * @param element: 元素
     * @return {@link int}
     * @date 2021/7/23 1:21
     **/
    @Override
    public int indexOf(E element) {
        Node<E> node = first;
        if (element == null) {
            for (int index = 0; index < size; index++) {
                if (node.element == null) {
                    return index;
                }
                node = node.next;
            }
        } else {
            for (int index = 0; index < size; index++) {
                if (node.element.equals(element)) {
                    return index;
                }
                node = node.next;
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 获得元素最后一次出现的索引
     *
     * @param element: 元素
     * @return {@link int}
     * @date 2021/7/23 1:24
     **/
    @Override
    public int lastIndexOf(E element) {
        int index = ELEMENT_NOT_FOUND;
        Node<E> node = first;
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (node.element == null) {
                    index = i;
                }
                node = node.next;
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (node.element.equals(element)) {
                    index = i;
                }
                node = node.next;
            }
        }
        return index;
    }

    /**
     * 清空链表
     *
     * @date 2021/7/23 1:14
     **/
    @Override
    public void clear() {
        size = 0;
        this.first = null;
    }

    private void rangeCheck(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index is: " + index + ", but size is: " + size);
        }
    }

    private void rangeCheckForInsert(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index is: " + index + ", but size is: " + size);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(node(i).element);
        }
        return "RbotoLinkedList{size=" + size + ", elements=[" + sb + "]}";
    }

    private static class Node<E> {
        private E element;
        private Node<E> next;

        public Node(E element, Node<E> next) {
            this.element = element;
            this.next = next;
        }

        public Object getElement() {
            return element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
}
