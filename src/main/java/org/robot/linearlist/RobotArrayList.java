/* Copyright 2021 - 2021 siyingcheng@126.com All rights reserved. */

package org.robot.linearlist;

/**
 * @author : Si YingCheng
 * @version : v0.0.1
 * @since : 2021/4/22 20:32
 **/
public class RobotArrayList<E> implements IDynamicLinearTable<E> {
    private static final int DEFAULT_CAPACITY = 10;
    private static final int ELEMENT_NOT_FOUND = -1;
    transient Object[] elements;
    private int size;

    /**
     * 空参构造器
     */
    public RobotArrayList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * 指定容量的构造器
     *
     * @param capacity 容量大小
     */
    public RobotArrayList(int capacity) {
        if (capacity > DEFAULT_CAPACITY) {
            this.elements = new Object[capacity];
        } else {
            this.elements = new Object[DEFAULT_CAPACITY];
        }
    }

    /**
     * 返回Array长度
     *
     * @return int
     */
    @Override
    public int size() {
        return this.size;
    }

    /**
     * 获取指定位置的元素
     *
     * @param index Array索引
     * @return E
     * @throws IndexOutOfBoundsException index >= size
     */
    @Override
    public E get(int index) {
        rangeCheck(index);
        return elements(index);
    }

    @SuppressWarnings("unchecked")
    E elements(int index) {
        rangeCheck(index);
        return (E) elements[index];
    }

    @Override
    public void add(E element) {
        insert(size, element);
    }

    /**
     * 给指定位置设置元素
     *
     * @param index   索引
     * @param element 要设置的对象
     * @return index设置前的对象
     * @throws IndexOutOfBoundsException index >= size
     */
    @Override
    public E set(int index, E element) {
        rangeCheck(index);
        E obj = elements(index);
        elements[index] = element;
        return obj;
    }

    /**
     * 给指定位置添加元素
     *
     * @param index   索引
     * @param element 要设置的元素
     * @throws IndexOutOfBoundsException index >= size
     */
    @Override
    public void insert(int index, E element) {
        rangeCheckForInsert(index);
        ensureCapacity(size + 1);
        if (size - index >= 0) System.arraycopy(elements, index, elements, index + 1, size - index);
        size++;
        elements[index] = element;
    }

    private void ensureCapacity(int capacity) {
        if (elements.length >= capacity) {
            return;
        }
        int oldCapacity = elements.length;
        // 扩容 1.5 倍
        // >> 优先级比较低 所以需要括号
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        Object[] newElements = new Object[newCapacity];
        System.arraycopy(elements, 0, newElements, 0, oldCapacity);
        elements = newElements;
    }

    private void rangeCheck(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index is: " + index + ", but size is: " + size);
        }
    }

    private void rangeCheckForInsert(int index) {
        if (index > size || index < 0) {
            throw new IndexOutOfBoundsException("Index is: " + index + ", but size is: " + size);
        }
    }

    /**
     * 删除制定索引的元素
     *
     * @param index 索引
     * @return 删除的元素
     */
    @Override
    public E remove(int index) {
        rangeCheck(index);
        E element = elements(index);
        System.arraycopy(elements, index + 1, elements, index, size - 1 - index);
        elements[--size] = null;
        return element;
    }

    /**
     * 删除元素element
     *
     * @param element: 要删除的元素
     * @date 2021/7/22 1:12
     **/
    @Override
    public void remove(E element) {
        remove(indexOf(element));
    }

    /**
     * 删除所有element
     *
     * @param element: 要删除的元素
     * @date 2021/7/22 1:12
     **/
    @Override
    public void removeAll(E element) {
        if (element == null) {
            for (int index = 0; index < size; index++) {
                if (elements[index] == null) {
                    remove(index);
                    index--;
                }
            }
        } else {
            for (int index = 0; index < size; index++) {
                if (elements[index].equals(element)) {
                    remove(index);
                    index--;
                }
            }
        }
    }

    /**
     * 是否为空
     *
     * @return 是否为空
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * 判断元素是否在列表中
     *
     * @param element 指定元素
     * @return 元素是否在列表中
     */
    @Override
    public boolean contains(E element) {
        return indexOf(element) >= 0;
    }

    /**
     * 判断指定元素在线性表中的索引
     *
     * @param element 元素
     * @return 索引, 如果没找到就返回-1
     */
    @Override
    public int indexOf(E element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (elements[i].equals(element)) {
                    return i;
                }
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 从后往前判断指定元素在线性表中的索引
     *
     * @param element 元素
     * @return 索引, 如果没找到就返回-1
     */
    @Override
    public int lastIndexOf(E element) {
        if (element == null) {
            for (int i = size - 1; i >= 0; i--) {
                if (elements[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = size - 1; i >= 0; i--) {
                if (elements[i].equals(element)) {
                    return i;
                }
            }
        }
        return ELEMENT_NOT_FOUND;
    }

    /**
     * 清空列表
     */
    @Override
    public void clear() {
        for (int i = 0; i < size; i++) {
            elements[i] = null;
        }
        size = 0;
    }

    public int getCapacity() {
        return elements.length;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (i != 0) {
                sb.append(", ");
            }
            sb.append(elements[i]);
        }
        return "RobotArrayList{size=" + size + ", elements=[" + sb + "]}";
    }
}
