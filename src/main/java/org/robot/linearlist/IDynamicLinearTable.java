/* Copyright 2021 - 2021 siyingcheng@126.com All rights reserved. */

package org.robot.linearlist;

/**
 * @author : Si YingCheng
 * @version : v0.0.1
 * @since : 2021/4/22 20:35
 **/
public interface IDynamicLinearTable<E> {
    /**
     * 返回Array长度
     * @return 返回Array长度
     */
    int size();

    /**
     * 获取指定位置的元素
     * @param index Array索引
     * @return Object
     */
    E get(int index);

    /**
     * 在最后添加元素
     *
     * @param element: 要添加的元素
     * @date 2021/7/20 23:42
     **/
    void add(E element);

    /**
     * 给指定位置设置元素
     * @param index 索引
     * @param element 要设置的对象
     * @return index设置前的对象
     */
    E set(int index, E element);

    /**
     * 给指定位置添加元素
     * @param index 索引
     * @param element 要设置的元素
     */
    void insert(int index, E element);

    /**
     * 删除制定索引的元素
     * @param index 索引
     * @return 删除的元素
     */
    E remove(int index);

    /**
     * 删除所有element
     *
     * @param element: 要删除的元素
     * @date 2021/7/22 0:59
     **/
    void remove(E element);

    /**
     * 删除所有element
     *
     * @param element: 要删除的元素
     * @date 2021/7/22 1:11
     **/
    void removeAll(E element);

    /**
     * 是否为空
     * @return 是否为空
     */
    boolean isEmpty();

    /**
     * 判断元素是否在列表中
     * @param element 指定元素
     * @return 元素是否在列表中
     */
    boolean contains(E element);

    /**
     * 判断指定元素在线性表中的索引
     * @param element 元素
     * @return 索引
     */
    int indexOf(E element);

    /**
     * 从后往前判断指定元素在线性表中的索引
     * @param element 元素
     * @return 索引
     */
    int lastIndexOf(E element);

    /**
     * 清空列表
     */
    void clear();
}
