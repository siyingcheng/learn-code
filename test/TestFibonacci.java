/* Copyright 2021 - 2021 siyingcheng@126.com All rights reserved. */


import org.robot.fibonacci.Fibonacci;
import org.robot.utils.TimeUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Optional;

/**
 * @author : Si YingCheng
 * @version : v0.0.1
 * @since : 2021/4/20 01:46
 **/
public class TestFibonacci {
    @Test(testName = "斐波那契-递归")
    public void testFibByRecursion() {
        Assert.assertEquals(Fibonacci.fibByRecursion(5), 5);
    }

    @Test(testName = "斐波那契-循环")
    public void testFibByLoop() {
        Assert.assertEquals(Fibonacci.fibByLoop(5), 5);
    }

    @Test(testName = "斐波那契-两种方法计算结果相同")
    public void testFibEquals() {
        Assert.assertEquals(Fibonacci.fibByLoop(10), Fibonacci.fibByRecursion(10));
    }

    @Test(testName = "斐波那契-线性代数解法")
    public void testFibByEquation() {
        Assert.assertEquals(Fibonacci.fibByLoop(10), Fibonacci.fibByEquation(10));
    }

    @Test(testName = "斐波那契-线性代数解法 性能测试")
    public void testFibByEquation2() {
        Optional<?> fibByEquation = TimeUtils.run("fibByEquation", () -> Optional.of(Fibonacci.fibByEquation(100)));
        fibByEquation.ifPresent(System.out::println);
    }

    @Test(testName = "斐波那契-fibByLoop 性能测试")
    public void testFibByLoop2() {
        Optional<?> fibByEquation = TimeUtils.run("fibByLoop", () -> Optional.of(Fibonacci.fibByLoop(100)));
        fibByEquation.ifPresent(System.out::println);
    }
}
